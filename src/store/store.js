/** REDUX **/
import {configureStore, getDefaultMiddleware, combineReducers} from "@reduxjs/toolkit";

/** REDUCERS **/
import {authReducer} from "./slices/authSlice";
import {testReducer} from "./slices/testSlice";

/** SLICES **/

/** PRE-LOADED STATE **/
// const preloadedState = {};

/** REDUCERS **/
const appReducer = combineReducers({
    auth: authReducer,
    test: testReducer,
});

const rootReducer = (state, action) => {
    if (action.type === 'auth/setAuthFalse') {
        state = undefined;
    }
    return appReducer(state, action)
};

/** MIDDLEWARE **/
const middleware = [
    ...getDefaultMiddleware(),
];

/** STORE **/
const store = configureStore({
    reducer: rootReducer,
    middleware,
    devTools: true,
    // preloadedState,
});
export default store;

