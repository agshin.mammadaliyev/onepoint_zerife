/** DEPENDENCIES **/
import {createSlice} from "@reduxjs/toolkit";

/** SLICE *************************************************************************************************************/
const testSlice = createSlice({
    name: 'test',
    initialState: null,
    reducers: {
        setTest(state, action){
            return action.payload;
        },
    }
});

/** EXPORT ACTIONS AND REDUCER ****************************************************************************************/
export const {setTest} = testSlice.actions;
export const testReducer = testSlice.reducer;