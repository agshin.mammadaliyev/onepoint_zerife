/** DEPENDENCIES **/
import {createSlice} from "@reduxjs/toolkit";

/** SLICE *************************************************************************************************************/
const authSlice = createSlice({
    name: 'auth',
    initialState: null,
    reducers: {
        setAuth(state, action){
            return action.payload;
        },
    }
});

/** EXPORT ACTIONS AND REDUCER ****************************************************************************************/
export const {setAuth} = authSlice.actions;
export const authReducer = authSlice.reducer;