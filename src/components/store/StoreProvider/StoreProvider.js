/** DEPENDENCIES **/
import React from 'react';
import {Provider} from 'react-redux';
import store from '../../../store/store';

/** COMPONENT *********************************************************************************************************/
const StoreProvider = ({children}) => {

    /** OUTPUT ********************************************************************************************************/
    return (
        <Provider store={store}>
            {children}
        </Provider>
    )
};

export default StoreProvider;