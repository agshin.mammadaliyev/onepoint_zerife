/** DEPENDENCIES **/
import React from 'react';
import './button.css';
import ElementLoader from "./ElementLoader/ElementLoader";

/** DICTIONARIES ******************************************************************************************************/
export const ButtonIconPosition = {
    left: 'left',
    right: 'right',
    center: 'center',
};

/** BUTTON COMPONENT **************************************************************************************************/
const Button = (props) => {
    /** PROPS **/
    const {
        htmlId = null,
        className = null,
        reference = null,
        tabIndex = null,
        label = 'Button',
        icon = null,
        iconPosition = (ButtonIconPosition.left),
        disabled = false,
        hidden = false,
        isLoading = false,
        onClick = (() => undefined),
    } = props;

    /** DISABLED **/
    const isDisabled = (isLoading || hidden) ? true : disabled;

    /** ELEMENT STYLES **/
    let classNames = require('classnames');
    const styles = classNames(
        'button',
        className,
        {
            [`icon-position-${iconPosition}`]: icon && iconPosition,
            'disabled': isDisabled,
            'hidden': hidden,
            'loading': isLoading,
        }
    );

    /** OUTPUT ********************************************************************************************************/
    return (
        <button
            id={htmlId}
            className={styles}
            ref={reference}
            tabIndex={tabIndex}
            disabled={isDisabled}
            onClick={onClick}
        >
            {
                <span className={'button-caption'}>
                    {icon ? <span className={'button-caption-icon'}>{icon}</span> : ''}
                    {label ? <span className={'button-caption-label'}>{label}</span> : ''}
                </span>
            }
            {isLoading ? <ElementLoader/> : ''}
        </button>
    )
};

export default Button;

// const Usage = () => {
//     return (
//         <Button
//             htmlId={null}
//             className={null}
//             reference={null}
//             tabIndex={null}
//             label={'Button'}
//             icon={null}
//             iconPosition={ButtonIconPosition.left}
//             disabled={false}
//             hidden={false}
//             isLoading={false}
//             onClick={() => undefined}
//         />
//     );
// };

