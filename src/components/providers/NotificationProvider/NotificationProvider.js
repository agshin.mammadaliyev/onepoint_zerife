/** DEPENDENCIES **/
import React from 'react';
import {useNotification} from "./useNotification/useNotification";
import {NotificationContext} from "./NotificationContext";
import Slider from "./Slider/Slider";

/** PROVIDES NOTIFICATION SLIDER **************************************************************************************/
const NotificationProvider = ({children}) => {

    /** NOTIFICATION HOOK **/
    const [notification, setNotification, clearNotification] = useNotification();

    /** VALUE FOR CONTEXT **/
    const providedValue = {
        setNotification: setNotification,
        clearNotification: clearNotification,
    };

    /** OUTPUT ********************************************************************************************************/
    return (
        <NotificationContext.Provider value={providedValue}>
            <Slider
                active={notification ? 2 : 1}
                list={[
                    children,
                    notification,
                ]}
            />
        </NotificationContext.Provider>
    )
};

export default NotificationProvider;

// const Usage = () => {
//     /** NOTIFICATION CONTEXT **/
//     const {setNotification, clearNotification} = useContext(NotificationContext);
//     return (
//         <NotificationProvider>
//             Children
//         </NotificationProvider>
//     )
// };
