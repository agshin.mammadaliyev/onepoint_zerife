/** DEPENDENCIES **/
import React from 'react';

/** COMPONENT *********************************************************************************************************/
const Track = ({children, ...props}) => {
    /** PROPS *********************************************************************************************************/
    const {
        active,
        length,
        transition,
    } = props;

    /** CUSTOM CSS STYLES **/
    const styles = {
        marginLeft: (-(active - 1) * 100) + '%',    //  Active slide is shown by shifting the track by left margin
        width: (length * 100) + '%',                //  The width is equal to sum of all slides' width
        transitionDuration: transition,             //  The duration of sliding transition
    };

    /** OUTPUT ********************************************************************************************************/
    return (
        <div className={'track'} style={styles}>
            {children}
        </div>
    )
};

export default Track;