/** DEPENDENCIES **/
import {useEffect} from "react";

/** THE HOOK SCROLLS UP TO TOP OF THE WINDOW ON TRIGGER CHANGE ********************************************************/
export const useScrollUp = (trigger, enabled = true) => {
    /** SCROLL UP ******************************************************************************************************
     *  -  Scrolls the page up when enabled if scrollUp is set to TRUE (by general is TRUE)
     */
    useEffect(() => {
        if (enabled) {
            window.scrollTo(0, 0);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [trigger]);
};

// const Usage = () => {
//     let active = 1;
//     let scrollUp = true;
//     /** SCROLL UP HOOK **/
//     useScrollUp(active, scrollUp);
// };
