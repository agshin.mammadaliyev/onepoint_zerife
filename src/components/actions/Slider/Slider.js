/** DEPENDENCIES **/
import React from 'react';
import './slider.css';
import Slide from "./structure/Slide/Slide";
import Track from "./structure/Track/Track";
import {useScrollUp} from "./useScrollUp/useScrollUp";

/** SLIDER *************************************************************************************************************
 *      - Component with slider effect.
 *      - Has several slides, that are rendered at same time, but are shown one by one.
 *      - You should define "List" (slides list) and "Active" (active slide).
 *      - Slides are numbered by integers starting from 1.
 *      - "Active" props defines which slide is active by its number
 */
const Slider = (props) => {
    /** PROPS *********************************************************************************************************/
    const {
        active,                 //  Active slide
        list,                   //  Array of slides
        htmlId = null,         //  HTML id
        className = null,      //  Custom class names
        scrollUp = true,       //  Behaviour on slide change: is optional, the slides are scrolled up by general
        transition = '300ms',   //  Slide transition duration: is optional and is equal to 300ms by default
    } = props;

    /** SCROLL UP HOOK ************************************************************************************************/
    useScrollUp(active, scrollUp);

    /** ELEMENT STYLES *************************************************************************************************
     *   classNames merges class names to one string
     */
    let classNames = require('classnames');
    const classNameList = classNames(
        'slider',
        className,
    );

    /** OUTPUT ********************************************************************************************************/
    return (
        <div id={htmlId} className={classNameList}>
            <Track
                active={active}
                length={list.length}
                transition={transition}
            >
                {list.map((slide, index) => {
                    return (
                        <Slide
                            key={index}                             //  Unique key
                            number={index + 1}                      //  Slide number (integer)
                            active={active}                         //  Active slide number (integer)
                            content={slide}                         //  Slide content
                            width={(100 / list.length) + '%'}       //  Slide width
                            transition={transition}
                        />
                    )
                })}
            </Track>
        </div>
    );
};

export default Slider;

// const Usage = () => {
//     return (
//         <Slider
//             active={1}
//             list={[
//                 <div>1</div>,
//                 <div>2</div>,
//                 // ...
//             ]}
//             htmlId={null}
//             className={null}
//             scrollUp={true}
//             transition={'300ms'}
//         />
//     )
// };

