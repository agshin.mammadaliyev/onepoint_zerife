/** DEPENDENCIES **/
import React from 'react';

/** COMPONENT *********************************************************************************************************/
const Slide = (props) => {
    /** PROPS *********************************************************************************************************/
    const {
        number,
        active,
        content,
        width,
        transition,
    } = props;

    /** ELEMENT STYLES *************************************************************************************************
     *   classNames merges class names to one string
     */
    let classNames = require('classnames');
    const classNameList = classNames(
        'slide',
        {
            [`slide-${number}`]: number,
            'active': number === active,
        }
    );

    const styles = {
        width: width,
        transitionDuration: transition,
        transitionDelay: transition/2,
    };

    /** OUTPUT ********************************************************************************************************/
    return (
        <div
            className={classNameList}
            style={styles}
        >
            {number === active ? content : ''}
        </div>
    )
};

export default Slide;