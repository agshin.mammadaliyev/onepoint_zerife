/** DEPENDENCIES **/
import React from 'react';
import Loader from "./Loader/Loader";
import SPINNER from './spinner.gif';
import './main-loader.css';

/** COMPONENT *********************************************************************************************************/
const MainLoader = () => {

    /** OUTPUT ********************************************************************************************************/
    return (
        <Loader spinner={SPINNER} className={'main-loader'}/>
    )
};

export default MainLoader;

// const Usage = () => {
//    return <MainLoader/>
// };