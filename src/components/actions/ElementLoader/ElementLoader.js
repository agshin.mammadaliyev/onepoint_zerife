/** DEPENDENCIES **/
import React from 'react';
import Loader from "./Loader/Loader";
import SPINNER from './spinner.gif';
import './element-loader.css';

/** COMPONENT *********************************************************************************************************/
const ElementLoader = () => {

    /** OUTPUT ********************************************************************************************************/
    return (
        <Loader spinner={SPINNER} className={'element-loader'}/>
    )
};

export default ElementLoader;

// const Usage = () => {
//    return <ElementLoader/>
// };