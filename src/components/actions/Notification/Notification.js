/** DEPENDENCIES **/
import React from "react";
import './notification.css';
import Button from "./Button/Button";

/** DICTIONARIES ******************************************************************************************************/
export const NotificationTypes = {
    info:'info',
    error:'error',
    success:'success',
    warning:'warning',
};

/** NOTIFICATION COMPONENT ********************************************************************************************/
export const Notification = (props) => {
    /** PROPS **/
    const {
        className = null,
        text = 'Default notification text',
        type = (NotificationTypes.info),
        button = '',
        onClick = (() => {return undefined;}),
    } = props;

    /** ICON **/
    const insertIcon = (type) => {
        switch (type) {
            case 'info':
                return 'i';
            case 'success':
                return <>&#x2713;</>;
            case 'error':
            default:
                return '!';
        }
    };

    /** OUTPUT ********************************************************************************************************/
    let classNames = require('classnames');
    return (
        <div className={classNames(
            'notification',
            className,
            type,
        )}>
            <div className={'notification-icon'}>{insertIcon(type)}</div>
            <p className={'notification-text'}>{text}</p>
            {button
                ?
                <Button
                    className={'notification-button'}
                    tabIndex={0}
                    label={button}
                    onClick={() => onClick()}
                />
                :
                ''}
        </div>
    )
};
export default Notification;

// const Usage = () => {
//     return (
//         <Notification
//             className={null}
//             text={'Default notification text'}
//             type={NotificationTypes.info}
//             button={''}
//             onClick={() => undefined}
//         />
//     );
// };

