/** DEPENDENCIES **/
import React, {useEffect} from 'react';
import {Route, Redirect} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import MainLoader from "../../actions/MainLoader/MainLoader";
import {useNotification} from "../../../hooks/useNotification/useNotification";
import {getAccessToken} from "../../../functions/token";
import {useRequest} from "../../../axios/axios";
import {setAuth} from "../../../store/slices/authSlice";
import {NotificationTypes} from "../../actions/Notification/Notification";

/** COMPONENT *********************************************************************************************************/
const PublicRoute = ({children, restricted = false, ...rest}) => {

    /** STORE **/
    const dispatch = useDispatch();
    let auth = useSelector(store => store.auth);

    /** NOTIFICATION HOOK **/
    const [notification, setNotification] = useNotification();

    /** AXIOS HOOKS ***************************************************************************************************/
    const [responseDetails, requestDetails, resetDetails] = useRequest();

    useEffect(() => {
        switch (responseDetails.status) {
            case 0:
                if (restricted) {
                    if (auth !== true && auth !== false) {
                        if (getAccessToken()) {
                            requestDetails(true);
                        }
                    }
                }
                break;
            case -1:
                break;
            case 200:
                dispatch(setAuth(true));
                resetDetails();
                break;
            case 401:
                break;
            case 404:
                dispatch(setAuth(false));
                resetDetails();
                break;
            case -2:
            default:
                setNotification('Xəta baş verdi!', NotificationTypes.error, 'Davam et', () => resetDetails());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [responseDetails.status]);


    /** OUTPUT ********************************************************************************************************/
    const CHILDREN = <Route {...rest} render={() => children}/>;
    const DASHBOARD = <Redirect push to="/dashboard"/>;
    return (
        notification ? notification : /** show notification if any **/
            !restricted ? CHILDREN : /** show the page if not restricted **/
                auth === false ? CHILDREN : /** show the page if auth was checked as false **/
                    auth === true ? DASHBOARD : /** redirect to dashboard if auth was checked as true **/
                        !getAccessToken() ? CHILDREN : /** show the page if no token in LS **/
                            <MainLoader/> /** show the loader if any token in LS until auth check **/
    );
};

export default PublicRoute;