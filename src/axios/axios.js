/** Axios request hook **/
import {useState} from "react";

export const endpointConfigs = (endpoint, payload) => {
    const endpoints = {
        login: {
            method: 'post',
            url: `/user/login`,
            baseURL: 'http://localhost:9000',
            isFormData: false,
            timeout: 12000,
            data: payload,
        },
        logout: {
            method: 'post',
            url: `/user/logout`,
            baseURL: 'http://localhost:9000',
            isFormData: false,
            timeout: 12000,
            data: payload,
        }
    };
    return endpoints[endpoint];
};




export const useRequest = () => {

    const [response, setResponse] = useState({
        data: null,
        status: 0,
    });

    function request(payload) {
        setResponse({
            data: null,
            status: -1,
        });
        setTimeout(returnResponse(payload), 5000);
    }

    function returnResponse(payload) {
        if(payload){
            setResponse({
                data: {someData: {}},
                status: 200,
            });
        } else if(payload === false){
            setResponse({
                data: null,
                status: 404,
            });
        } else {
            setResponse({
                data: null,
                status: 503,
            });
        }
    }

    function resetRequest() {
        setResponse({
            data: null,
            status: 0,
        })
    }

    /** OUTPUT ********************************************************************************************************/
    return [
        response,
        (payload) => request(payload),
        () => resetRequest(),
    ]
};