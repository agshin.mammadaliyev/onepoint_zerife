/** DEPENDENCIES **/
import React from 'react';

/** COMPONENTS **/
import Dispatcher from "./app/Dispatcher";

/** PROVIDERS **/
import StoreProvider from "./components/store/StoreProvider/StoreProvider";
import RouterProvider from "./components/routing/RouterProvider/RoterProvider";
import Test from "./app/Test";

/** COMPONENT *********************************************************************************************************/
function App() {
    return (
        <StoreProvider>
            <RouterProvider>
                <Test/>
            </RouterProvider>
        </StoreProvider>
    );
}

export default App;