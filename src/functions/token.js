/** DEPENDENCIES **/
import backup from "./backup"

/** TOKEN FUNCTIONS ***************************************************************************************************/

/** Get access token from backup service **/
export const getAccessToken = () => {
    return backup('token');
};

/** Get refresh token from backup service **/
export const getRefreshToken = () => {
    return backup('refresh');
};

/** Get url token from url query **/
export const getTokenFromURL = () => {
    let queryString = window.location.search;
    if (queryString.indexOf('?token=') !== -1 && queryString.length >= 200) {
        return queryString.substring(queryString.indexOf('?token=') + 7);
    } else {
        return '';
    }
};