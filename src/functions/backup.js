const backup = (element = null) => {
    let bcp;
    try {
        const serializedState = localStorage.getItem('backup');
        if (!serializedState) {
            bcp = {};
        } else {
            bcp = JSON.parse(serializedState);
        }
    } catch (e) {
        bcp = {};
    }
    return element === null ? bcp : bcp[element];
};

export default backup;

