/** DEPENDENCIES **/
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

/** APPLICATION'S ENTRY POINT *****************************************************************************************/
ReactDOM.render(
    <React.StrictMode>
            <App/>
    </React.StrictMode>,
    document.getElementById('root')
);