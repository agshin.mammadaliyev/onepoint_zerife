/** DEPENDENCIES **/
import React, {useRef} from 'react';
import Button from "../components/form/buttons/Button/Button";
import {useDispatch, useSelector} from "react-redux";
import axios from 'axios';
import {setTest} from "../store/slices/testSlice";
import {endpointConfigs} from "../axios/axios";

/** COMPONENT *********************************************************************************************************/
const Test = () => {

    const dispatch = useDispatch();
    // const data = useSelector(state => state.test);

   function request(payload) {
       axios(endpointConfigs('login', payload))
           .then(response => {
               dispatch(setTest(response.data));
           })
           .catch(error => {
               console.log(error.response.status);
               console.log(error.response.data);
           })
   }

    const login = useRef();
    const password = useRef();

    /** OUTPUT ********************************************************************************************************/
    return (
        <div style={{display:'flex', flexDirection: 'column'}}>
            <input ref={login} type={'text'} style={{padding: '0.5em', margin: '1em 0'}}/>
            <input ref={password} type={'text'} style={{padding: '0.5em', margin: '1em 0'}}/>
            <Button
                label={'Button'}
                onClick={() => request({
                    username: login.current.value,
                    secret: password.current.value,
                })}
            />
        </div>
    )
};

export default Test;