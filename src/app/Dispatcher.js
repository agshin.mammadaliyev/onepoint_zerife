/** DEPENDENCIES **/
import React from 'react';

/** ROUTER **/
import {Switch, Redirect} from "react-router-dom";

/** COMPONENTS **/
import PublicRoute from "../components/routing/PublicRoute/PublicRoute";
import PrivateRoute from "../components/routing/PrivateRoute/PrivateRoute";
import NotificationProvider from "../components/providers/NotificationProvider/NotificationProvider";
import Test from "./Test";

/** COMPONENT *********************************************************************************************************/
const Dispatcher = () => {

    /** OUTPUT ********************************************************************************************************/
    return (
        <>
            <NotificationProvider>
                <Switch>
                    <PublicRoute exact path="/" restricted={true}>
                        <Redirect push to={'/login'}/>
                    </PublicRoute>
                    <PublicRoute path="/login" restricted={true}>
                        <Test/>
                    </PublicRoute>
                    <PublicRoute path="/registration" restricted={true}>
                        <h2>Registration</h2>
                    </PublicRoute>
                    <PublicRoute path="/reset-password" restricted={true}>
                        <h2>Reset Password</h2>
                    </PublicRoute>
                    <PrivateRoute path={'/dashboard'}>
                        <h2>Dashboard</h2>
                    </PrivateRoute>
                    <PublicRoute path="/public-not-restricted" restricted={false}>
                        <h2>Not restricted page</h2>
                    </PublicRoute>
                    <PublicRoute path="/" restricted={false}>
                        <h2>Error 404 OnePoint</h2>
                    </PublicRoute>
                </Switch>
            </NotificationProvider>
        </>
    )
};

export default Dispatcher;