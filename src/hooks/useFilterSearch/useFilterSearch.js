/** DEPENDENCIES **/
import React, {useEffect} from 'react';
import {useHistory, useLocation} from "react-router-dom";

/** COMPONENT *********************************************************************************************************/
export const useFilterSearch = () => {
    /** FILTER SEARCH PARAMETERS IN URL *******************************************************************************/
    const location = useLocation();
    const history = useHistory();

    /** CHECK SEARCH STRING IN URL **/
    useEffect(() => {
        /** IF THERE IS A SEARCH STRING **/
        if (location.search) {
            /** IS IT TOKEN **/
            if (location.search.substring(0, 7) !== '?token=') {
                /** NOT TOKEN -> DELETE SEARCH STRING **/
                history.push(location.pathname);
            } else {
                /** IS TOKEN **/
                if (!['/registration', '/reset-password'].includes(location.pathname)) {
                    /** PATH SHOULDN'T HAVE TOKEN -> DELETE SEARCH STRING **/
                    history.push(location.pathname);
                } else {
                    // if is Register or Recover page - doesn't delete the token line in url for further authentication
                }
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location.search]);
};