/** DEPENDENCIES **/
import React from 'react';
import './loader.css';

/** COMPONENT *********************************************************************************************************/
const Loader = (props) => {
    /** PROPS **/
    const {
        spinner = null,
        description = '',
        alt = 'Loading...',
        className = null,
    } = props;

    /** OUTPUT ********************************************************************************************************/
    return (
        <div className={'loader ' + className}>
            <img
                className={'loader-spinner'}
                src={spinner}
                alt={alt}
            />
            {
                description
                    ? <p className={'loader-description'}>{description}</p>
                    : ''
            }
        </div>
    )
};

export default Loader;

// const Usage = () => {
//     return (
//         <Loader
//             spinner={null}
//             description={''}
//             alt={''}
//         />
//     )
// };