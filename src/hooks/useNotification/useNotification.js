/** DEPENDENCIES **/
import React, {useState} from "react";
import Notification from "./Notification/Notification";

/** THE HOOK RETURNS NOTIFICATION COMPONENT ***************************************************************************/
export const useNotification = () => {
    /** LOCAL STATE **/
    const [notification, setNotification] = useState('');

    /** OUTPUT ********************************************************************************************************/
    return [
        /** OUTPUT COMPONENT **/
        notification,
        /** FUNCTION SETS NOTIFICATION TO OUTPUT **/
        ((text, type, button = null, sideEffect = (() => 'undefined'), className = null) => {
            setNotification(
                <Notification
                    className={className}
                    text={text}
                    type={type}
                    button={button}
                    onClick={() => {
                        setNotification('');
                        if (typeof sideEffect === 'function') sideEffect();
                    }}
                />
            );
        }),
        /** FUNCTION RESETS OUTPUT **/
            (sideEffect = null) => {
            setNotification('');
            if (typeof sideEffect === 'function') sideEffect();
        },
    ];
};

// const Usage = () => {
//     /** NOTIFICATION HOOK **/
//     const [notification, setNotification, clearNotification] = useNotification();
// };